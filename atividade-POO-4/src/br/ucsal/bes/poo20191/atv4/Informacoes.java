package br.ucsal.bes.poo20191.atv4;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public static class Informacoes {
	static Scanner sc = new Scanner(System.in);

	String[] nome = new String[50];
	String[] telefone = new String[50];
	Integer[] anoNas = new Integer[50];
	String dataNas = new Date[50];
	String[] tipos = new String[50];

	public static void contatos() {

		for (int i = 0; i <= 1; i++) {
			System.out.println("Nome: ");
			nome[i] = sc.nextLine();
			System.out.println("Telefone: ");
			telefone[i] = sc.nextLine();
			System.out.println("Ano de Nascimento: ");
			anoNas[i] = sc.nextInt();
			System.out.println("Data de nascimento: ");
			try {

				System.out.println("Digite uma data: ");
				String dataNas = sc.nextLine();
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				Date dt = df.parse(dataNas);
				System.out.println(dt);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.out.println("Pessoal ou Profissional ?");
			sc.nextLine();
			tipos[i] = sc.nextLine();

		}

	}

	public static void excluir() {

		String delete;

		System.out.println("qual contato deseja excluir");
		delete = sc.nextLine();

		for (int i = 0; i < nome.length; i++) {
			if (nome[i].equals(delete)) {

				nome[i] = "";
				telefone[i] = "";
				anoNas[i] = null;
				dataNas[i] = "";
				tipo[i] = "";

			}
		}

		System.out.println("Contato excluido com sucesso");

	}

	public static void listar() {

		for (int i = 0; i < nome.length; i++) {

			System.out.println(nome[i]);

		}

	}

	public static void pesquisar() {

		String pesquisar;

		pesquisar = sc.nextLine();

		for (int i = 0; i < nome.length; i++) {
			if (nome[i].equals(pesquisar)) {

				System.out.println(nome[i]);
				System.out.println(telefone[i]);
				System.out.println(anoNas[i]);
				System.out.println(dataNas[i]);
				System.out.println(tipo[i]);
			}

		}
	}

	public static void escolhas() {

		System.out.print("\n");
		System.out.println("1 - Incluir");
		System.out.println("2 - Excluir");
		System.out.println("3 - Listar");
		System.out.println("4 - Pesquisar por nome de contato");
		System.out.println("5 - Sair da agenda");

	}
}
